# La question

+ Une présentation intelligible du projet et de ses intentions ;
+ Une présentation technique et précise de la manière dont vous envisagez de
réaliser votre projet :
+ Techniques d’impression utilisées (riso, comcolor, laser, gravure),
	combinaisons éventuelles,
+ Nombre d’exemplaires, idées de façonnage... ;
+ Un extrait de votre projet ou, idéalement, le projet complet ;
+ Une petite idée, si vous en avez, d’un projet d’exposition ou de mise en
espace ;
+ Une présentation de vous, de votre travail, tout ce qui nous permettrait de
mieux vous connaître et comprendre vos intentions !

# La réponse

Salut ! On est Lucinne et Thomas, ancien·nes étudiant·es de l'erg, puis à l'ESA
St Luc et à la Cambre. On est tout·es les deux fasciné·es par les techniques
d'impression, on a d'ailleurs fait partie du groupe des réplicant·es de l'erg
qui s'occupent de la riso de l'école.

Lucinne est illustratrice/autrice de BD et actuellement dans l'attente de
publier sa première BD, *Gary et Joon*. Elle aime dessiner à la ligne et les
couleurs vives (surtout le rouge). Elle oriente sa pratique  vers un public
jeunesse et aimerait faire des ateliers avec des enfants.

Thomas est graphiste/webdesigner. Il s'intéresse aux modes d'emploi, à la
documentation, aux formes hybrides de publication entre le papier et le
numérique. Il utilise les logiciels libres et la programmation pour concevoir
ses outils ou bricoler des outils existants.

## Projet et intentions

On aime beaucoup jouer à des jeux de société, mais on était un peu frustré·es
de ne pas connaître plus de jeux permettant de jouer à 2 avec un jeu de cartes
"classiques" (à part la bataille bien sûr mais c'est vraiment pas amusant). Un
jour on est tombé·es sur un [jeu
italien](https://fr.wikipedia.org/wiki/Jeu_de_cartes_italien), beaucoup plus
illustré que les cartes à jouer belges ou françaises. On a alors commencé
à jouer à la [scopa](https://fr.wikipedia.org/wiki/Scopa_(jeu_de_cartes)), un
jeu très connu en Italie qui a la particularité de se jouer à 2 ou
à 4 (parfait).

Pour ce premier projet commun après la sortie de nos études, on s'est dit que
le jeu italien offrait plus de possibilités de s'amuser avec les illustrations
qu'un jeu belge/français, et que ça permettrait de faire découvrir d'autres
jeux de cartes de l'Europe (on peut aussi jouer à la briscola, autre jeu
italien, ou encore des jeux espagnols ou suisses avec ces cartes) !

Notre projet est une adaptation des cartes traditionnelles italiennes, en
enlevant les symboles guerriers, masculins, classistes. On a décidé de
remplacer le valet, le cavalier et le roi par celles des figures de
l’évolution, du plus petit au plus grand ! De la graine à la fleur, de l’oeuf
à la grenouille, du bébé à la grand-mère et pour finir du volcan au soleil. Ces
quatre familles remplacent les deniers, les coupes, les épées, et les bâtons du
jeu initial. Les illustrations sont faites par Lucinne et la mise en page et le
façonnage par Thomas.

Les 4 nouvelles familles du jeu de carte sont donc :
+ Les grenouilles
+ La terre/minéraux (l'équivalent des deniers)
+ Les fleurs
+ Les humain·es

## Présentation technique

### Logiciels

Autant que possible, des logiciels libres sont utilisés pour la production du
jeu (Inkscape, Scribus, Gimp, Krita…).

### Impression

Pour l'instant on est en riso 2 passages. Chaque carte a 1 passage de noir pour
le dessin au trait (probablement au laser)
+ 2 passages de couleurs dont une principale (qu'on retrouve sur l'encadrement
  des cartes et permet de différencier les familles rapidement).

On aimerait utiliser les 4 couleurs teal / jaune / violet / rose pour les
4 familles, avec une couleur secondaire pour créer des mélanges en riso.

Et donc on aurait cette répartition (plus du noir sur chaque famille) :
+ GRENOUILLES → **violet** + teal
+ FLEURS → **teal** + jaune
+ HUMAIN·ES → **rose** + violet
+ MINÉRAUX → **jaune** + rose

Les cartes sont suffisamment petites et peu nombreuses pour que 2 familles
puissent tenir dans un A3, ce qui est tout à fait raisonnable comme format
d'impression en riso. Puisqu'il y a des couleurs en commun entre plusieurs
familles, ça permet d'économiser les masters en imprimant d'un coup une même
couleurs pour 2 familles de cartes.

Pour le verso des cartes, un fonc rouge serait imprimé puis un motif commun
serait rajouté en noir.

Il faut aussi imprimer des règles du jeu, qui peuvent être imprimée avec une
autre technique que la riso (laser, comcolor) et sur un papier plus fin pour
être facilement plié.

### Papier/façonnage

Puisqu'on fait un jeu de cartes, il faut que le papier soit suffisamment épais
pour qu'on puisse jouer plus que 5 parties avant qu'elles ne tombent en
miettes, mais suffisamment fin (et non couché) pour qu'on puisse l'imprimer
dans une riso.

Une autre possibilité est d'utiliser un papier avec un grammage relativemement
faible, mais de le contrecoller avec un film plastique. Ça aura le double
avantage de rigidifier la carte et de la protéger (surtout que le jeu est fait
pour être beaucoup manipulé et que l'encre de riso n'aime pas trop rester en
place sur le papier).

Le reste du façonnage serait de couper au massicot toutes les cartes et
d'arrondir les angles pour une finition carte à jouer.

Enfin, pour la boîte, on utiliserait un papier plus épais (300 g/m2) qui n'a
pas besoin nécessairement de passer à la riso. On pensait en fait utiliser les
marges des cartes pour imprimer des décors qui seront ensuite collés sur la
boîte. Grâce à cette méthode on pourrait avoir une boîte très colorée et
décorée sans utiliser de master de riso en plus, et on aurait une boîte plus
rigide.

Le patron des boîtes sera découpé sur notre plotter pour gagner en temps et en
précision. Avec le plotter on peut également marquer les plis pour avoir un
résultat vraiment propre.

On aimerait imprimer entre 50 et 100 exemplaires, compte tenu du budget
d'impression et du temps qu'on aurait en résidence.

## Idées pour l'exposition

Pour le vernissage on pensait organiser des tables de jeu ou les personnes qui
viennent pourraient jouer avec nous/entre elleux. On pourrait faire des
affiches ou des présentoirs avec les règles de différents jeux que l'on peut
faire avec ces cartes.

Pour habiller un peu plus l'espace on imprimerait certaines cartes (les plus
illustrées, les cartes 8-9-10 de chaque famille) en "grand format" (A3, grand
par rapport à la taille des cartes). On ferait aussi des pyramides avec des
cartes pour garder le côté ludique et original.
