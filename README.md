# Scopacabana

Un jeu de scopa sans les figures classistes/patriarcales/guerrières mais avec
des figures de l'évolution des choses de la nature. Dessins de [Lucinne Salva](https://www.instagram.com/lucinnesalva/).

## 4 familles
+ Les grenouilles
+ Les fleurs
+ Les humain·es
+ Les minéraux (famille principale, l'équivalent des pièces d'or)

## Les règles du jeu
(à écrire encore, mais ça doit tenir sur une petite carte ou 2)

## Logiciels
Autant que possible, des logiciels libres sont utilisés pour la production du
jeu (Inkscape, Scribus, Gimp, Krita, CartaGenius)

## Impression
En théorie en riso 3 passages. Chaque carte a 1 passage de noir pour le dessin
au trait + 2 passages de couleurs dont une principale.

bleu / jaune / vert / rose → à voir en fonction des dispos de là où on va imprimer

Et donc :
+ GRENOUILLES → **bleu** + jaune
+ FLEURS → **vert** + bleu
+ HUMAIN·ES → **rose** + vert
+ MINÉRAUX → **jaune** + rose
